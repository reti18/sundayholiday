# sundayHoliday
Progetto di Reti di Calcolatori AA 2018/19, corso tenuto dal prof. Andrea Vitaletti [https://github.com/andreavitaletti/RC]
La documentazione completa è disponibile nella Sezione Wiki del progetto, al link[https://gitlab.com/reti18/sundayholiday/wikis/home]
## Abstract
Il nostro progetto consiste in una applicazione web per organizzare gite ed eventi con altre persone, raccogliendo le informazioni sui luoghi come meteo, data, valutazione degli utenti, link utili, ecc.
## Servizi esterni
I servizi esterni usati sono:
- Google Places (da cui prendiamo le informazioni di base sui luoghi e valutazioni degli utenti)
- Facebook Graph (dal quale prendiamo foto e il link della pagina facebook relativa a quel luogo)
- OpenWeather (dal quale prendiamo il meteo di quel luogo)
- Google Calendar (sul quale salviamo gli eventi, previa autorizzazione dell'utente tramite OAuth)
## Strumenti usati
Gli strumenti software utilizzati sono invece:
- docker (creazione di container, ambienti virtuali minimali più efficienti delle VM)
- nginx (risorse statiche e load balancer)
- node (risorse dinamiche)
	- due istanze di node si occupano della logica applicativa distribuendosi il lavoro
	- un'istanza si occupa di gestire la chat con le web socket
- express (gestione grafica le pagine web)
- passport (automatizza le procedure di autenticazione con OAuth, sia con google che con couchDB)
- couchDB (salvataggio utenti ed eventi)
- amqp (commenti sugli eventi)
- web socket (chat globale)
## API
Le API che mettiamo a disposizione consistono in:
- Creare un evento
- Eliminare un evento
- Visualizzare un evento

La struttura delle API nel dettaglio è visualizzabile qui: https://app.swaggerhub.com/apis/fcolasante/sundayHoliday/1.0.0

# Usage e Avvio
Per avviare il progetto, seguire i seguenti step:

0. Clonare la repository
   ```s
		git clone https://gitlab.com/reti18/sundayholiday.git
		cd sundayholiday
   ```	
  1. Registrarsi ai vari servizi esterni e generare i vari token
  2. Modificare il file `secret.example.js` in `secret.js`, modificando i vari parametri adeguatamente
  3. Installare Docker e Docker-compose
```s
	docker-compose up
```	